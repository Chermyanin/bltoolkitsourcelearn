﻿using System.Collections.Generic;
using System.Linq;
using BLToolkit.Data;

namespace BlToolkitTrace
{
    public class TraceQueries 
    {
        public static IEnumerable<T> Get<T>(string connection,int limit) where T: class
        {
            using (var dbManager = new DbManager(connection))
            {
                return  dbManager.GetTable<T>().Take(limit).ToList();
            }
        }
    }
}
