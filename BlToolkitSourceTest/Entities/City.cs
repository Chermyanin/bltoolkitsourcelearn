﻿using BLToolkit.DataAccess;

namespace BlToolkitSourceTest.Entities
{
    [TableName("cities")]
    public class City
    {
        public int CityId { get; set; }
        public string CountryId { get; set; }
        public bool Important { get; set; }
        public int RegionId { get; set; }
        public string Title { get; set; }
        public string Area { get; set; }
        public string Region { get; set; }
    }
}
