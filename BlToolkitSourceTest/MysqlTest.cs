﻿using System.Collections.Generic;
using System.Linq;
using BlToolkitSourceTest.Entities;
using NUnit.Framework;
using BLToolkit.Data;

namespace BlToolkitSourceTest
{
    [TestFixture]
    public class MysqlTest
    {
        [TestFixtureSetUp]
        public void SetUpFixture()
        {
            log4net.Config.XmlConfigurator.Configure();
        }
        [Test]
        public void Should_Select_From_Table()
        {
            const string connection = "DefaultConnection";
            IEnumerable<City> result;
            using(var dbManager = new DbManager(connection))
            {
                result = dbManager.GetTable<City>().Take(2).ToList();
            }
            Assert.That(result,Is.Not.Empty);
        }

        [Test]
        public void Should_Get_With_After_Query_Predicate()
        {
            var result = _getCities().Where(el => el.Title.StartsWith("Мос")).Take(5).ToList();
            Assert.That(result,Is.Not.Empty);
        }

        [Test]
        public void Should_Get_With_Before_Predicate()
        {
            const string connection = "DefaultConnection";
            IEnumerable<City> result;
            using(var dbManager = new DbManager(connection))
            {
                result = dbManager.GetTable<City>().Where(el => el.Title.StartsWith("Мос")).Skip(5).Take(5).ToList();
            }
            Assert.That(result,Is.Not.Empty);
        }

        private IEnumerable<City> _getCities()
        {
            const string connection = "DefaultConnection";
            IEnumerable<City> result;
            using(var dbManager = new DbManager(connection))
            {
                result = dbManager.GetTable<City>();
            }
            return result;
        }

        [Test,Ignore]
        public void Should_Get_Association()
        {
            const string connection = "Shard_1_Connection";
            using(var dbManager = new DbManager(connection))
            {
            }
        }

        [Test]
        public void Should_Get_Linq_Expression()
        {
            const string connection = "DefaultConnection";
            IEnumerable<City> result;
            using(var dbManager = new DbManager(connection))
            {
                var query = from c in dbManager.GetTable<City>()
                            where c.Title.StartsWith("Мос")
                            select c;
                result = query.Skip(4).Take(5).ToList();
            }
            Assert.That(result,Is.Not.Empty);
        }
    }
}
